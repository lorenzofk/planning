(define (domain lighting)
	(:requirements 
		:strips 
		:typing 
		:negative-preconditions
	)

	(:predicates  
		(stairs ?str)
		(hallway ?h)
		(room ?rm)
		(deposit ?dpt)
		(lamp ?lmp)
		(free ?rbt)
		(forgotStairs ?rbt)
		(robot ?rbt)
		(hasLamp ?rbt ?lmp)
		(hasStairs ?rbt ?str)
		(at ?rm ?rbt)
		(lampOn ?rm ?lmp)
		(stairsIsMounted ?rm ?stairs)
	)

	(:action moveToRoom
		:parameters(?from ?to ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?from ?agent)
				(hallway ?from)
				(room ?to)
			)
		:effect
			(and
				(at ?to ?agent)
				(not (at ?from ?agent))
			)
	)

	(:action moveToDeposit
		:parameters(?from ?to ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?from ?agent)
				(hallway ?from)
				(deposit ?to)
			)
		:effect
			(and
				(at ?to ?agent)
				(not (at ?from ?agent))
			)
	)

	(:action moveToHallway
		:parameters(?from ?to ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?from ?agent)
				(not(hallway ?from))
				(hallway ?to)
				(not (forgotStairs ?agent))
			)
		:effect
			(and
				(at ?to ?agent)
				(not (at ?from ?agent))
			)
	)

	(:action getLamp
		:parameters(?local ?l ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?local ?agent)
				(deposit ?local)
				(lamp ?l)
				(not (hasLamp ?agent ?l))
				(free ?agent)
			)
		:effect
			(and
				(hasLamp ?agent ?l)
				(not(free ?agent))
			)
	)

	(:action getStairs
		:parameters(?local ?s ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?local ?agent)
				(deposit ?local)
				(stairs ?s)
				(not (hasStairs ?agent ?s))
			)
		:effect
			(and
				(hasStairs ?agent ?s)
			)
	)

	(:action  mountStairs
		:parameters(?local ?s ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?local ?agent)
				(room ?local)
				(stairs ?s)
				(hasStairs ?agent ?s)
				(not (stairsIsMounted ?local ?s))
			)
		:effect
			(and
				(stairsIsMounted ?local ?s)
			)
	)

	(:action  unmountStairs
		:parameters(?local ?s ?agent)
		:precondition
			(and
				(robot ?agent)
				(at ?local ?agent)
				(room ?local)
				(stairs ?s)
				(stairsIsMounted ?local ?s)
			)
		:effect
			(and
				(not(forgotStairs ?agent))
				(not(stairsIsMounted ?local ?s))
			)
	)

	(:action installLamp
		:parameters(?local ?agent ?l ?s)
		:precondition
			(and
				(robot ?agent)
				(at ?local ?agent)
				(room ?local)
				(lamp ?l)
				(stairs ?s)
				(hasLamp ?agent ?l)
				(hasStairs ?agent ?s)
				(not (lampOn ?local ?l))
				(stairsIsMounted ?local ?s)
			)
		:effect
			(and
				(lampOn ?local ?l)
				(not (hasLamp ?agent ?l))
				(free ?agent)
				(forgotStairs ?agent)
			)
	)
)