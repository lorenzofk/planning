(define (problem pb1)
    (:domain lighting)
	(:requirements :strips :typing)
  	(:objects hal9000 way roomA roomB roomC roomD depot lamp1 lamp2 lamp3 lamp4 Stairs)


	(:init 
		(robot hal9000)
		(hallway way)
		(room roomA)
		(room roomB)
		(room roomC)
		(room roomD)
		(deposit depot)
		(lamp lamp1)
		(lamp lamp2)
		(lamp lamp3)
		(lamp lamp4)
		(stairs Stairs)
		(free hal9000)
		(not(forgotStairs hal9000))

		(at roomA hal9000)
	)
	
	(:goal 
		(and 
			(lampOn roomA lamp1)
			(lampOn roomB lamp2)
			(lampOn roomC lamp3)
			(lampOn roomD lamp4) 
		)
	)
)
